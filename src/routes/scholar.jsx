import Home from "../views/Scholar/Home";
import Search from "../views/Scholar/Search";

export default [
    { path: '/scholar',component: Home, exact: true },
    { path: '/scholar/search',component: Search }
]