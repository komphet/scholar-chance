import {SET_LOADING, SET_IS_LOADING} from "../constants/actionTypes";


const initialState = {
    isLoading: true,
    active: true,
    animate: true,
    spinner: true,
    text: "Loading...",
    background: "black",
    color: "white",
    spinnerSize: "100px",
    onClick: null,
    zIndex: 900,
    style: {
        position: "fixed",
        width: "100%",
        height: "100%",
    }
};

export default function reducer(state = initialState, action) {
    switch (action.type) {
        case SET_LOADING: {
            let style;
            if(action.payload.active){
                style = {
                    position: "fixed",
                    width: "100%",
                    height: "100%",
                    // zIndex: 900,
                }
            } else {
                style = {};
            }
            return {
                ...state,
                ...action.payload,
                style
            }
        }
        case SET_IS_LOADING: {
            return {
                ...state,
                isLoading: action.payload,
            }
        }
        default:
            return state

    }
}
