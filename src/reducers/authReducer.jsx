import {LOGIN_SUCCESS, LOGOUT_SUCCESS} from '../constants/actionTypes'

const initialState = {
    token: "",
    isLogin: false,
};

export default function reducer(state = initialState, action) {
    let {payload} = action;
    switch (action.type) {
        case LOGIN_SUCCESS :
            return {
                ...state,
                token: payload,
                isLogin: true,
            };
        case LOGOUT_SUCCESS :
            return {
                initialState
            };
        default:
            return state
    }
}
