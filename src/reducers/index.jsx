import {combineReducers} from 'redux';
import auth from './authReducer';
import loading from './loadingReducer'
import profile from './profileReducer'
import filter from './filterReducer'
import item from './itemReducer'
export default combineReducers({
    auth,loading,profile,filter,item
})
