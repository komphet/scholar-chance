import {CLEAR_FILTER, SET_FILTER} from "../constants/actionTypes";

const initialState = {
    query: "",
    type: "",
    countries: [],
    educationLevel: "",
    educationYear: "",
    faculty: "",
    branch: "",
    grade: "",
    budget: "",
    needSupports: [],
    interestedFields: [],
    documents: [],
    englishTestScore: [],
};

export default function reducer(state = initialState, action) {
    switch (action.type) {
        case SET_FILTER :
            return {
                ...state,
                ...action.payload
            };
        case CLEAR_FILTER :
            return {
                initialState
            };
        default:
            return state
    }
}
