import {ADD_ITEM, REMOVE_ITEM} from '../constants/actionTypes'

const initialState = {
  count: 0
};

export default function reducer(state = initialState, action) {
  let {payload} = action;
  switch (action.type) {
    case ADD_ITEM :
      console.log(state.count);
      if(state.count > 5){
        alert("สามารถเลือกได้ไม่เกิน 5 รายการเท่านั้น");
        return state;
      }
      return {
        ...state,
        count: parseInt(state.count) +1
      };
    case REMOVE_ITEM :
    console.log(state.count);
      return {
        ...state,
        count: parseInt(state.count) -1
      };
    default:
      return state
  }
}
