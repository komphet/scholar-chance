import {SET_FILTER} from "../constants/actionTypes";

export function setFilter(data){
    return (dispatch)=>{
        dispatch({
            type:SET_FILTER,
            payload: data
        })
    }
}