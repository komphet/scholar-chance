import {ADD_ITEM, REMOVE_ITEM} from "../constants/actionTypes";

export function addItem(){
  return (dispatch)=>{
    dispatch({
      type:ADD_ITEM
    })
  }
}
export function removeItem(){
  return (dispatch)=>{
    dispatch({
      type: REMOVE_ITEM
    })
  }
}