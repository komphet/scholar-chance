import {SET_IS_LOADING, SET_LOADING, SET_STREAMING} from "../constants/actionTypes";

export function loading(active, text = "Loading...") {
    return (dispatch) => {
        dispatch({
            type: SET_LOADING,
            payload: {
                active,
                text
            }
        });
    }
}


export function setIsLoading(active){
    return (dispatch,getState)=>{
        if(active && !getState().loading.isLoading){
            dispatch({
                type:SET_IS_LOADING,
                payload:{
                    active
                }
            })
        }
    }
}
