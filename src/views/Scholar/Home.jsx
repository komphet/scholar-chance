import React, {Component} from 'react';
import {
  Container,
  Row,
  Col
} from 'reactstrap'
import {NavLink} from "react-router-dom";

import HomeSeacrh from '../../components/Home/HomeSearch'
import ServiceBox from '../../components/Home/ServiceBox'

import CourseImg from '../../static/img/course.jpg'
import DocumentImg from '../../static/img/document.png'
import VideoImg from '../../static/img/video.png'

import Background from '../../static/img/background-1.jpg'

class Home extends Component {
  render() {
    return (<>
      <div style={{background: "#000"}}>
        <div className="bg-image mb-3" style={{backgroundImage: `url(${Background})`, backgroundSize: "cover"}}>
          <Container>
            <br/>
            <HomeSeacrh {...this.props} />
            <br/>
            <br/>
          </Container>
        </div>
      </div>
      <br/>
      <br/>
      <br/>
      <Container className="margin-200">
        <Row>
          <Col xs={12}>
            <div className="title-border">
              <hr/>
              <h1 className="text" style={{ minWidth: 300, textAlign: 'center' }}>3 บริการสู่ทุนที่ใช่</h1>
            </div>
          </Col>
          <Col xs={12} md={4}>
            <NavLink to={"/service/grammar"} className="link">
              <ServiceBox img={DocumentImg}>
                <div className="text-center">
                  <p>ตรวจสอบความถูกต้อง</p>
                  <p>มั่นใจทุกเอกสาร ผ่านผู้เชี่ยวชาญตัวจริง</p>
                </div>
              </ServiceBox>
            </NavLink>
          </Col>
          <Col xs={12} md={4}>
            <NavLink to={"/service/consult"} className="link">
              <ServiceBox img={VideoImg}>
                <div className="text-center">
                  <p>ปรึกษารุ่นพี่ที่มีประสบการณ์ตรงผ่าน VDO call</p>
                </div>
              </ServiceBox>
            </NavLink>
          </Col>
          <Col xs={12} md={4}>
            <NavLink to={"/service/course"} className="link">
              <ServiceBox img={CourseImg}>
                <div className="text-center">
                  <p>อัพสกิลผ่านคอร์สเรียนยอดนิยมจากหลายสถาบัน</p>
                </div>
              </ServiceBox>
            </NavLink>
          </Col>
        </Row>
      </Container>
    </>)
  }
}

export default Home;