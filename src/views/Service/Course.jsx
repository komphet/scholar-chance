import React, {Component} from 'react';
import ServiceBox from '../../components/Home/ServiceBox'
import Footer from "../../components/Footer";

import Course1 from '../../static/img/course1.png'
import Course2 from '../../static/img/course2.png'
import Course3 from '../../static/img/course3.png'

class Course extends Component {
  render() {
    return (
        <div>
          <div style={{height: "3em"}}/>
          <h1>คอร์สแนะนำสำหรับคุณ</h1>
          <div className="row">
            <div className="col-lg-3 col-md-6 col-12">
              <a href="https://www.britishcouncil.or.th/" target={"_blank"}>
                <div className="card service-box my-3">
                  <div className="card-body">
                    <img
                        src={Course1}
                        className="img-fluid" alt=""/>
                    <div className="service-content text-center">
                      <h3>IELTS Extension</h3>
                      <p>โดย New Cambridge</p>
                    </div>
                  </div>
                </div>
              </a>
            </div>
            <div className="col-lg-3 col-md-6 col-12">
              <a href="https://www.chulatutor.com/toefl" target={"_blank"}>
                <div className="card service-box my-3">
                  <div className="card-body">
                    <img
                        src={Course2}
                        className="img-fluid" alt=""/>
                    <div className="service-content text-center">
                      <h3>TOEFL แบบตัวต่อตัว</h3>
                      <p>โดย Chulatutor</p>
                    </div>
                  </div>
                </div>
              </a>
            </div>
            <div className="col-lg-3 col-md-6 col-12">
              <a href="http://englishparks.in.th/ourcourse/academic-writing" target={"_blank"}>
                <div className="card service-box my-3">
                  <div className="card-body">
                    <img
                        src={Course3}
                        className="img-fluid" alt=""/>
                    <div className="service-content text-center">
                      <h3>Academic Writing สำหรับ IELTS</h3>
                      <p>โดย Englishparks</p>
                    </div>
                  </div>
                </div>
              </a>
            </div>
          </div>
          {/* <h1>คุณอาจสนใจคอร์สเหล่านี้ด้วย</h1>
          <div className="row">
            {[1, 2, 3, 4].map(() => {
              return (
                  <div className="col-lg-3 col-md-6 col-12">
                    <a href="https://www.britishcouncil.or.th/" target={"_blank"}>
                      <div className="card service-box my-3">
                        <div className="card-body">
                          <img
                              src={"https://secure.skypeassets.com/content/dam/scom/skype-features/skype-video-call.jpg"}
                              className="img-fluid" alt=""/>
                          <div className="service-content text-center">
                            <h3>IELTS Extension</h3>
                            <p>โดย New Cambridge</p>
                          </div>
                        </div>
                      </div>
                    </a>
                  </div>
              )
            })}
          </div>
          <div className="row">
            <div className="col-sm-6">
              <a href={"https://google.com/search?q=เรียนภาษาอังกฤษ"}  target={"_blank"} className="btn btn-primary" style={{width: "100%"}}>ดูคอร์สทั้งหมด</a>
            </div>
            <div className="col-sm-6">
              <a  href={"https://google.com/search?q=เรียนภาษาอังกฤษ"}  target={"_blank"} className="btn btn-primary" style={{width: "100%"}}>ดูตัวเลือกอื่น</a>
            </div>
          </div> */}
          <div style={{height: "5em"}}/>
        </div>
    )
  }
}

export default Course;