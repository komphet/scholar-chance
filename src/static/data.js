import Scholar1 from '../static/img/scholar-1.png'
import Scholar2 from '../static/img/scholar-2.png'
import Scholar3 from '../static/img/scholar-3.png'

export default {
  scholarship: [
    {
      id: 1,
      name: 'Boustany MBA -Harvard Business School',
      amount: '45,000 USD/ปี', // ราคา
      max: '1 ทุน',
      startdate: "ภายในวันที่ 31 พ.ค.62",
      country: 'USA',
      gpaxRequired: 3.00,
      thumbnail: Scholar1,
      type: 3,
      major: 'MBA'
    },
    {
      id: 2,
      name: "MBA - Newcastle Business University",
      max: 'ไม่ระบุ',
      startdate: 'ภายในวันที่ 5เม.ย.62',
      country: 'ENG',
      gpaxRequired: 3.00,
      thumbnail: Scholar2,
      type: 3,
      amount: '£ 6,000 – 21,600', // ราคา
      major: 'MBA'
    },
    {
      id: 3,
      name: "MBA - Newcastle Business University",
      amount: '50% ค่าเล่าเรียน',
      max: '1 ทุน',
      startdate: 'ภายในวันที่28เม.ย.62',
      country: 'ENG',
      gpaxRequired: 3.00,
      thumbnail: Scholar3,
      type: 2,
      major: 'Human Resourses'
    }
  ]
}