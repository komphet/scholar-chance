import Sidebar from './Sidebar'
import React, { Component } from 'react';


class Header extends Component {

  render() {
    return (
      <Sidebar {...this.props} />
    );
  }
}

export default Header