import React, { Component } from 'react'
import {
  Container,
  Row,
  Col,
  Input,
  FormGroup,
  Label,
  Form,
  Button
} from 'reactstrap';


class HomeSearch extends Component {

  handleSubmit = (event) => {
    event.preventDefault()

    console.log('submit')
    this.props.history.push('/scholar/search')
  }


  render() {
    return (
      <>
        <Row className="justify-content-center">
          <Col xs={12} md={12}>
            <div className="d-flex justify-content-center search-center-box">
              <h1 style={{fontSize: "5em"}} className="text-orange headline">
                The best chance <br />
                You can have  
              </h1>
              <Row className="mt-4">
                <Col xs={12} md={4}>
                  <div className="border-bottom"></div>
                  <p className="text-white" style={{ marginTop: '20px',fontSize: "1.5em" }} >มาเพิ่มโอกาสได้ทุนป.โท ไปกับเรา</p>
                      {/* <Label>เลือกประเทศ</Label> */}
                  <Form className="w-100" onSubmit={this.handleSubmit}>
                    <FormGroup>
                      <select
                        type="text"
                        className="form-control form-control-lg button-radius"
                        name="country"
                        placeholder="ประเทศ"
                        required
                      >
                        <option value="">เลือกประเทศ...</option>
                        <option value="">อเมริกา</option>
                        <option value="">อังกฤษ</option>
                        <option value="">ออสเตรเลีย</option>
                      </select>
                    </FormGroup>
                    <FormGroup>
                      <select
                        type="text"
                        className="form-control form-control-lg button-radius"
                        name="major"
                        placeholder="สาขาเรียน"
                        required
                      >
                        <option value="">เลือกประเภททุน...</option>
                        <option value="">ทั้งหมด</option>
                      </select>
                    </FormGroup>
                    <div className="text-center">
                      <button className="btn btn-primary btn-lg button-radius bg-blue">ค้นหา</button>
                    </div>
                  </Form>
                </Col>
              </Row>

            </div>
          </Col>
        </Row>
      </>
    )
  }
}

export default HomeSearch
