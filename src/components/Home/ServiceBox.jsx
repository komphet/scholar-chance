import React, {Component} from 'react'
import {Link} from 'react-router-dom'

class ServiceBox extends Component {
    render() {
        return (
            <div className="service-box my-3">
                <div className="card-body">
                    <div className="circle-image" style={{ backgroundImage: `url(${this.props.img})` }}></div>
                    <div className="service-content">
                        {this.props.children}
                    </div>
                </div>
            </div>
        )
    }
}

export default ServiceBox
