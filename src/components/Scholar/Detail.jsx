import React, { Component } from 'react'
import {
  Container,
  Row,
  Col
} from 'reactstrap'
import { Link } from 'react-router-dom'

import HavardImge from '../../static/img/havard.png';
import {connect} from 'react-redux';
import {addItem} from "../../actions/itemActions";

import CourseImg from '../../static/img/course.jpg'
import DocumentImg from '../../static/img/document.png'
import VideoImg from '../../static/img/video.png'

class Detail extends Component {
  render() {
    return (
      <div className="full-modal">
        <Row>
          <Col>
            <div className="modal-header">
              Scholarship Detail
              <button onClick={this.props.closeModal} type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <Container>
                <Row>
                  <Col className="px-0 px-md-2">
                    <div className="pull-right">
                      <button onClick={this.props.addItem.bind(this)} className="btn btn-warning" style={{width: "50px",height: "50px", borderRadius: "50%", textAlign: "center"}}><i className="fa fa-download"/></button></div>
                    <h2>Harvard Business School Boustany MBA Scholarship</h2>
                    <Row className="justify-content-center">
                      <Col xs={12} md={6} >
                        <img src={HavardImge} className="img-fluid mb-3" alt="university"/>
                      </Col>

                    </Row>
                    <p>นักศึกษานานาชาติทั่วโลกที่สนใจศึกษาต่อระดับปริญญาโททางด้าน MBA สามารถชิงทุนการศึกษา The Boustany MBA Harvard Scholarship ได้แล้ววันนี้ มูลค่ารวมกว่า 3 ล้านบาท ครอบคลุมค่าเล่าเรียนและค่าใช้จ่ายทั้งหมด
                    </p>
                    <p>
      ทุนการศึกษานี้มอบให้ผู้สมัครทั่วโลกทุกสองปี สนับสนุนโดยกองทุน The Boustany Foundation โดยผู้ได้รับการคัดเลือกนั้นจะได้เข้าเรียนที่Harvard Business School ในระดับปริญญาโทเป็นเวลา 2 ปี พร้อมกับโอกาสฝึกงานในกองทุนเป็นเวลา 2 เดือนหลังเรียนจบ
                    </p>
                    <strong>คุณสมบัติของผู้สมัคร</strong>
                    <ol>
                      <li>ผู้สมัครมาจากสัญชาติใดก็ได้ไม่จำกัด</li>
                      <li>ต้องได้การตอบรับเข้าศึกษาจากทางมหาวิทยาลัยก่อนสมัคร</li>
                      <li>มีประวัติผลการศึกษาที่ยอดเยี่ยม และแสดงถึงความมุ่งมั่นในการเรียน</li>

                    </ol>
                    <strong>สิ่งที่ต้องใช้ยื่นสมัคร</strong>
                    <ol>
                      <li>Transcript ฉบับจริง</li>
                      <li>Resume</li>
                      <li>Letter of Recommendation จากผู้รับรอง 2 คน</li>
                      <li>Essays ตามหัวข้อที่มหาวิทยาลัยกำหนด</li>
                      <li>คะแนนทดสอบภาษาอังกฤษไม่ต่ำกว่าเกณฑ์ดังต่อไปนี้ TOEFL มากกว่า 109, IELTS มากกว่า 7.5 และคะแนน PTE ไม่ต่ำกว่า 75</li>
                    </ol>
                    <p>หากผู้สมัครมีคุณสมบัติครบถ้วนตามเกณฑ์ที่กำหนดไว้ จะได้รับการคัดเลือกเข้าสู่รอบสัมภาษณ์ และประกาศผลอีกครั้งภายในเดือนมิถุนายน พ.ศ. 2562</p>
                    <p>
                      <strong>หมายเหตุ</strong> แม้ทุนจะเปิดโอกาสให้นักศึกษาทั่วโลก แต่ผู้สมัครที่มีเชื้อสายเลบานอนจะได้รับการพิจารณาเป็นพิเศษ
                    </p>
                  </Col>
                </Row>
                <div style={{height: "2em"}}/>
                <h1>เพิ่มโอกาสติดทุนให้สูงขึ้นอย่างง่าย ๆ</h1>
                <h4>ด้วยตัวเลือกที่คัดสรรมาเพื่อช่วยเหลือคุณอย่างเต็มที่</h4>
                <div style={{height: "2m"}}/>
                <Link to="/service/grammar" className="link">
                  <div className="row py-3 border-bottom ">
                    <div className="col-sm-3">
                      <img src={CourseImg}
                      width={"100%"} alt={"Image"} />
                    </div>
                    <div className="col-sm-9">
                      <h1>1.ตรวจเอกสารอย่างมั่นใจ</h1>
                      <h4>ภาษาสละสลวย ถูกหลักไวยากรณ์ รวดเร็วดั่งใจ ด้วยผู้เชี่ยวชาญของเรา</h4>
                    </div>
                  </div>
                </Link>
                <Link to="/service/consult" className="link">
                  <div className="row py-3 border-bottom">
                    <div className="col-sm-3">
                      <img src={DocumentImg}
                          width={"100%"} alt={"Image"} />
                    </div>
                    <div className="col-sm-9">
                      <h1>2. ปรึกษารุ่นพี่แบบตัวต่อตัว</h1>
                      <h4>VDO Call ถามลึก รู้ทุกประเด็น มีลุ้นเรียนต่อชัวร์</h4>
                    </div>
                  </div>
                </Link>
                <Link to="/service/course" className="link">
                  <div className="row py-3 border-bottom">
                    <div className="col-sm-3">
                      <img src={VideoImg}
                          width={"100%"} alt={"Image"} />
                    </div>
                    <div className="col-sm-9">
                      <h1>3.เรียนคอร์สยอดฮิต</h1>
                      <h4>อัพสกิลกับกูรู สอบผ่านแบบฉลุยแน่นอน</h4>
                    </div>
                  </div>
                </Link>
              </Container>
            </div>
          </Col>
        </Row>
      </div>
    )
  }
}

export default connect(null,{addItem})(Detail);
