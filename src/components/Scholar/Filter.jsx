import React, {Component} from "react";

class Filter extends Component{

    render() {
        const { country1, country2, toggle } = this.props

        return (
            <div className="p-15">
                <select name="type" className="form-control">
                    <option value="">ประเภททุน</option>
                </select>
                <label htmlFor="countries[]">ประเทศ</label>
                <p><label><input name={"countries[]"} value={"USA"} onChange={() => { toggle('country1') }} type="checkbox" checked={country1}/> อเมริกา</label></p>
                <p><label><input name={"countries[]"} value={"ENG"} onChange={() => { toggle('country2') }} type="checkbox" checked={country2} /> อังกฤษ</label></p>
                <p><label><input name={"countries[]"} value={"ออสเครเลีย"} type="checkbox"/> ออสเตรเลีย</label></p>
                <select name="educationLevel" className="form-control">
                    <option value="">ระดับการศึกษาปัจจุบัน</option>
                    <option value="ปริญญาตรี">ปริญญาตรี</option>
                    <option value="ปริญญาโท">ปริญญาโท</option>
                </select>
                <select name="educationYear" className="form-control">
                    <option value="">ชั้นปี</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="สำเร็จการศึกษา">สำเร็จการศึกษา</option>
                </select>
                <input name="faculty" placeholder={"คณะ"} className="form-control"/>
                <input name="branch" placeholder={"สาขา"} className="form-control"/>
                <input name="grade" placeholder={"เกรด"} className="form-control"/>
                <input name="budget" placeholder={"งบประมาณที่ตั้งไว้"} className="form-control"/>
                <label htmlFor="needServices[]">ความสนับสนุนที่ต้องการ</label>
                <p><label><input name={"needServices[]"} value={"ตั๋วเครื่องบิน"} type="checkbox"/> ตั๋วเครื่องบิน </label></p>
                <p><label><input name={"needServices[]"} value={"ค่าที่พัก"} type="checkbox"/> ค่าที่พัก </label></p>
                <p><label><input name={"needServices[]"} value={"เงินสนับสนุนค่าใช้จ่ายรายเดือน"}
                                 type="checkbox"/> เงินสนับสนุนค่าใช้จ่ายรายเดือน </label></p>
                <p><label><input name={"needServices[]"} value={"ค่าเรียนปรับพื้นฐาน"}
                                 type="checkbox"/> ค่าเรียนปรับพื้นฐาน </label></p>
                <p><label><input name={"needServices[]"} value={"ประกันสุชภาพ"} type="checkbox"/> ประกันสุขภาพ </label></p>
                <label htmlFor="interestedFields[]">สาขาที่สนใจเรียนต่อ</label>
                <input name="interestedFields[]" className="form-control" value="MBA" />
                <input name="interestedFields[]" className="form-control" value="Human Resources" />
                <label htmlFor="documents[]">สิ่งที่คุณมี</label>
                <p><label><input name={"documents[0]"} id={"documents[0]"} value={"ตั๋วเครื่องบิน"}
                                 type="checkbox"/> SoPs</label></p>
                <p><label><input name={"documents[1]"} id={"documents[1]"} value={"ค่าที่พัก"}
                                 type="checkbox"/> CV</label></p>
                <p><label><input name={"documents[2]"} id={"documents[2]"}
                                 value={"เงินสนับสนุนค่าใช้จ่ายรายเดือน"} type="checkbox"/> Resume</label></p>
                <p><label><input name={"documents[3]"} id={"documents[3]"} value={"ค่าเรียนปรับพื้นฐาน"}
                                 type="checkbox"/> Essays</label></p>
                <p><label><input name={"documents[4]"} id={"documents[4]"} value={"ประกันสุขภาพ"}
                                 type="checkbox"/> Letter of Recommendation</label></p>
                <label htmlFor="interestedFields[]">คะแนนสอบวัดระดับภาษาอังกฤษ</label>
                <input name="englishTestScore[]" placeholder={"TOEFL"} className="form-control"/>
                <input name="englishTestScore[]" placeholder={"IELTS"} className="form-control"/>
                <input name="englishTestScore[]" placeholder={"GMAT"} className="form-control"/>
                <input name="englishTestScore[]" placeholder={"อื่นๆ"} className="form-control"/>
            </div>
        )
    }
}

export default Filter;