import React, { Component } from 'react'
import ReactSidebar from 'react-sidebar'
import { NavLink } from 'react-router-dom'
import {
  Navbar
} from 'reactstrap'
import Logo from '../static/img/logo.png'
import Footer from "../components/Footer";

const styles = {
    root: {
      position: 'fixed',
      top: 0,
      right: 0,
      bottom: 0,
      overflowX: 'hidden'
    },
    sidebar: {
      zIndex: 2,
      maxWidth: '500px',
      width: '60vw',
      position: 'fixed',
      backgroundColor: 'white',
      top: 56,
      bottom: 0,
      transition: 'right transform .3s ease-out',
      WebkitTransition: '-webkit-transform .3s ease-out',
      willChange: 'transform',
      overflowY: 'auto',
    },
    content: {
      position: 'absolute',
      top: 0,
      right: 0,
      bottom: 0,
      overflowY: 'auto',
      WebkitOverflowScrolling: 'touch',
      transition: 'right .3s ease-out, right .3s ease-out',
    },
    overlay: {
      zIndex: 1,
      position: 'fixed',
      top: 0,
      right: 0,
      bottom: 0,
      opacity: 0,
      visibility: 'hidden',
      transition: 'opacity .3s ease-out, visibility .3s ease-out',
      backgroundColor: 'rgba(0,0,0,.3)',
    },
    dragHandle: {
      zIndex: 1,
      position: 'fixed',
      top: 0,
      bottom: 0,
    },
  }

class SidebarContent extends Component {
  constructor(props){
    super(props);
    this.state = {
      isLogin: false
    }
  }

  login(){
    this.setState({
      isLogin: true
    });
  }

  logout(){
    this.setState({
      isLogin: false
    });
  }

  render() {
    return (
      <div className={"text-center"} style={{padding: "20px"}}>
        {this.state.isLogin ? (
            <div className={"text-center"}>
              <div style={{fontSize: "5em"}}><i className="fa fa-user-circle"/></div>

              <NavLink className="sidebar-link" to="/">ข้อมูลส่วนตัว</NavLink>
              <NavLink className="sidebar-link" to="/">ทุนที่ชื่นชอบ</NavLink>
              <a href={"#"} className="sidebar-link" onClick={this.logout.bind(this)}>ลงชื่อออก</a>
            </div>
        ) : (<div>
          <div style={{height: "2em"}}/>
          <input type="text" className="form-control button-radius" placeholder={"Email"}/>
          <div style={{height: "2em"}}/>
          <input type="password" className="form-control button-radius" placeholder={"Password"}/>
          <div style={{height: "2em"}}/>
          <button onClick={this.login.bind(this)} className="btn btn-info button-radius bg-blue" style={{width: "100%"}}>ลงชื่อเข้าใช้</button>
        </div>)}

      </div>
    )
  }
}

class Sidebar extends Component {
    state = {
        sidebarOpen: false
    }

    toggleSidebar = () => {
        this.setState({
            sidebarOpen: !this.state.sidebarOpen
        })
    }

    render () {
        return (
          <ReactSidebar
            // sidebar={this.props.user ? <MenuBar user={this.props.user} /> : <></>}
            open={this.state.sidebarOpen}
            onSetOpen={this.toggleSidebar}
            styles={styles}
            pullRight
            sidebar={<SidebarContent />}
            // touch={!this.props.user === false}
          >
            <Navbar className="fixed-top bg-gray">
                <NavLink to={"/"} style={{ textDecoration:"none"}}>
                  <img src={Logo} className="img-fluid" alt="" style={{ height: '40px' }}/>
                </NavLink>
            </Navbar>
            <button className="sidebar-burger-button" onClick={() => this.toggleSidebar()}>
                <div className="sidebar-burger-bar"></div>
                <div className="sidebar-burger-bar"></div>
                <div className="sidebar-burger-bar"></div>
            </button>
                        {/* {this.props.user
              ? <Bar>
                <a href='/'><Logo src='/static/img/logo.png' /></a>
                <BurgerBox onClick={() => this.toggleSidebar()}>
                  <BurgerBar />
                  <BurgerBar />
                  <BurgerBar />
                </BurgerBox>
              </Bar>
              : <Bar>
                <a href='/'><Logo src='/static/img/logo.png' /></a>
                <Link href='/login'>SIGN IN</Link>
              </Bar>
              } */}
            <div className="content-with-margin">
              {this.props.content}
              <Footer/>
            </div>
          </ReactSidebar>
        )
      }
}

export default Sidebar
