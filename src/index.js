import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import './styles/app.css';
import Root from './containers/App';
import * as serviceWorker from './serviceWorker';
import {
    HashRouter,
    BrowserRouter,
    Router
} from 'react-router-dom';
// import { BrowserRouter } from 'react-router-dom'

import {Provider} from 'react-redux';
import store from './store';
import history from './history';
import {loading} from "./actions/loadingActions";

class App extends Component {
    render() {
        return (
            <HashRouter>
                <Provider store={store}>
                    <Root/>
                </Provider>
            </HashRouter>
        )
    }
}

ReactDOM.render(<App/>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.register();
let _send = XMLHttpRequest.prototype.send;
let _sendCount = 0;
XMLHttpRequest.prototype.send = function () {
    if (_sendCount <= 0) {
        store.dispatch(loading(true));
    }

    console.log(_send.responseURL);

    _sendCount++;
    /* Wrap onreadystaechange callback */
    var callback = this.onreadystatechange;
    this.onreadystatechange = function () {
        if ((/.*sockjs-node.*/).test(this.responseURL)) {
            _sendCount--;
            if (_sendCount <= 0) {
                store.dispatch(loading(false));
            }
        }
        if (this.readyState === 4) {
            _sendCount--;
            if (_sendCount <= 0) {
                store.dispatch(loading(false));
            }
        }

        callback.apply(this, arguments);
    };

    _send.apply(this, arguments);

};


