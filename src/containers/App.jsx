import React, {Component} from "react";
import {
    Switch,
    Route,
    Redirect,
    withRouter
} from 'react-router-dom';
import Header from '../components/Header'
import appRoutes from '../routes/app';
import Loadable from 'react-loading-overlay';
import {connect} from "react-redux";
import Footer from '../components/Footer';


class App extends Component {
    render() {
        return (
            <>
                <Loadable {...this.props.loading} />
                <Header content={
                    <div style={{ marginTop: '56px' }}>
                        <Switch>
                            {
                                appRoutes.map((prop, key) => {
                                    if(!prop.redirectPath){
                                        return (
                                            <Route path={prop.path} component={prop.component} key={key}/>
                                        );
                                    } else {
                                        return <Redirect to={prop.redirectPath} key={key}/>
                                    }

                                })
                            }
                        </Switch>
                    </div>
                }>
                </Header>

            </>
        )
    }
}

export default withRouter(connect(store => {
    return {loading: store.loading}
})(App));
