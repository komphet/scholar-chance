import React, {Component} from 'react';
import {Redirect, Route, Switch} from "react-router-dom";
import serviceRoute from '../routes/service';

class Service extends Component {
  render() {
    return (
        <div>
          <div className={"container"}>
            <Switch>
              {
                serviceRoute.map((prop, key) => {
                  if (!prop.redirectPath) {
                    return (
                        <Route path={prop.path} component={prop.component} key={key}/>
                    );
                  } else {
                    return <Redirect to={prop.redirectPath}/>
                  }

                })
              }
            </Switch>
          </div>


        </div>
    )
  }
}

export default Service;