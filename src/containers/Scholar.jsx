import React, {Component} from 'react';
import appRoutes from "../routes/app";
import {Redirect, Route, Switch} from "react-router-dom";
import scholarRoute from "../routes/scholar";

class Scholar extends Component{
    render() {
        return (
            <div className={""}>
                <Switch>
                    {
                        scholarRoute.map((prop, key) => {
                            if(!prop.redirectPath){
                                return (
                                    <Route path={prop.path} component={prop.component} key={key} exact={!!prop.exact} />
                                );
                            } else {
                                return <Redirect to={prop.redirectPath}/>
                            }

                        })
                    }
                </Switch>
            </div>
        )
    }
}

export default Scholar;